// Cookie Handlers
export const cookie = {
	create: (name, value, days) => {
		var expires;

		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toGMTString();
		} else {
			expires = "";
		}
		document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
	},
	read: (name) => {
		var nameEQ = encodeURIComponent(name) + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) === ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
		}
		return null;
	},
	erase: (name) => {
		createCookie(name, "", -1);
	}
}

// Location
export const location = {
	query: (query) => {
		var vars = query.split("&");
		var query_string = {};
		for (var i = 0; i < vars.length; i++) {
			var pair = vars[i].split("=");
			if (typeof query_string[pair[0]] === "undefined") {
				query_string[pair[0]] = decodeURIComponent(pair[1]);
			} else if (typeof query_string[pair[0]] === "string") {
				var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
				query_string[pair[0]] = arr;
			} else {
				query_string[pair[0]].push(decodeURIComponent(pair[1]));
			}
		}
		return query_string;
	}
}

export const getUrlParams = url => {
  if (!url) {
    return { };
  }

  return [...new URL(url).searchParams].reduce((a, param) => {
    a[param[0]] = param[1].split(',')
    return a
  }, {})
}

export const arrayToObject = arr => (
  arr.reduce((a, item) => {
    a[item.name] = item.value
    return a
  }, {})
)

export const capitalize = string => {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
}
