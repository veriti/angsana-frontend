export default function(phrase) {
  const translations = window.BT_VOCABULARY || {}
  return translations[phrase] || phrase
}
