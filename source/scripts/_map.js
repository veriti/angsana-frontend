let _map = (function ($, w, evt) {
  var $viewer;
  var locations = [];
  var filters;
  var map;
  var mapConfig;
  var clusterSizes;
  var iconPath = 'assets/img/';
  var bounds;
  var markers;
  var markerClusterer;
  var view;
  var $map;
  var $mapContainer;
  var $mapDetails;

  function init(el) {
    $viewer = $('#location_view');
    view = w._switch.view();
    mapConfig = {
      zoom: 3,
      center: { lat: -28.024, lng: 140.887 },
      styles: mapTheme,
      mapTypeControl: false,
      fullscreenControl: false,
      streetViewControl: false
    }
    locations = w._data.get().data;
    clusterSizes = [50, 70, 85, 105, 120];
    iconPath = window.BTMAP_IMAGEPATH;
    markers = [];
    filteredLocations = locations;

    evt.on('filterUpdated', applyFilters);
    evt.on('viewChanged', applyView);
    evt.on('markerClicked', showDetails);
  }

  function onReady() {
    bounds = new google.maps.LatLngBounds();
  }

  function render() {
    if (view != 'map') return;

    if (!$viewer) return;

    $mapDetails = '';
    $mapContainer = $mapContainer || $('<div class="location"></div>');
    $map = $map || $('<div class="map location__map"></div>')

    $mapContainer.removeClass('location--detailed');
    $mapContainer.html($map);
    $mapDetails = $('<div class="location__detail"><button id="closeMapDetail" class="button"><i class="icon icon--ui-close-i"></i></button><header></header><article></article><footer><a href="#"><small>Discover more &gt;</small></a></footer></div>')

    if ($viewer.data('view-details')) {
      $mapContainer.append($mapDetails);
    }

    if ($map.children().length === 0) {
      map = new google.maps.Map($map.get(0), mapConfig);
    }

    $viewer.html($mapContainer);

    clearMarkers();
    setMarkers();
  }

  function setMarkers() {
    var clustererOptions = {
      styles: clusterSizes.map(function (size, i) {
        return {
          url: iconPath + 'm' + (i + 1) + '.png',
          height: size,
          width: size,
          textSize: 20 + (i * 2),
          textColor: 'white'
        }
      })
    }

    filteredLocations.map(function (location, i) {
      var marker = new google.maps.Marker({
        position: location.latLng,
        icon: iconPath + 'pin-' + location.brand + '.png'
      });

      var infoWindow = new google.maps.InfoWindow({
        content: location.title
      });

      marker.addListener('click', onMarkerClicked.bind(marker, location));
      marker.addListener('mouseover', function () {
        infoWindow.open(map, marker);
      });

      marker.addListener('mouseout', function () {
        infoWindow.close(map, marker);
      });

      bounds.extend(marker.getPosition());
      markers.push(marker);

      return marker;
    });

    markerClusterer = new MarkerClusterer(map, markers, clustererOptions);
    map.fitBounds(bounds);
    google.maps.event.trigger(map, 'resize');
  }

  function clearMarkers() {
    markers = [];
    markerClusterer && markerClusterer.clearMarkers();
  }

  function onMarkerClicked(location) {
    evt.trigger('markerClicked', location);
    google.maps.event.trigger(map, 'resize');
    map.panTo(this.getPosition());
    map.setZoom(14);
  }

  function applyFilters(f) {
    filters = f;

    filteredLocations = locations.reduce(reducer, []);

    render();
  }

  function applyView(config) {
    if (config) {
      view = config.view;
    }

    var poll = setInterval(function () {
      if (!google) {
        init();
        clearInterval(poll);
        return;
      }
    }, 1000);

    render();
  }

  function reducer(accumulator, location) {
    if (filters['brands'] && filters['brands'].indexOf(location['brand']) === -1) return accumulator;
    if (filters['countries'] && filters['countries'].indexOf(location['country']) === -1) return accumulator;
    if (filters['themes'] && filters['themes'].indexOf(location['theme']) === -1) return accumulator;
    if (filters['destinations'] && filters['destinations'].indexOf(location['destination']) === -1) return accumulator;

    accumulator.push(location);
    return accumulator;
  }

  function showDetails(location) {
    var $body = $('.location__detail article');
    var $head = $('.location__detail header');
    var $link = $('.location__detail footer a');
    var type = _data.get().type;

    $details = $('<div/>');
    $details.html(location.description ? '<p>'+location.description+'</p>' : '<p>'+location.excerpt+'</p>');

    if (type === 'event') {
      $details = $('<ul></ul>');
      var detailsHtml = '';
      detailsHtml += '<li>'+location.features.size_sqft+'sqft / '+ location.features.size_sqm+' m<sup>2</sup></li>';
      detailsHtml += '<li>'+location.features.delegates+' Persons Maximum</li>';
      detailsHtml += '<li>'+location.features.guest_rooms+' Guest Rooms</li>';
      detailsHtml += '<li>'+location.features.ballroom+' Ballroom</li>';
      detailsHtml += '<li class="list-inlne">';
      detailsHtml += location.features.roundtable_cap ? '<i class="icon icon--seats-a"></i>': '';
      detailsHtml += location.features.classroom_cap ? '<i class="icon icon--seats-b"></i>': '';
      detailsHtml += location.features.boardroom_cap ? '<i class="icon icon--seats-c"></i>': '';
      detailsHtml += location.features.freestand_cap ? '<i class="icon icon--seats-d"></i>': '';
      detailsHtml += location.features.theatre_cap ? '<i class="icon icon--seats-e"></i>': '';
      detailsHtml += location.features.ushape_cap ? '<i class="icon icon--seats-b"></i>': '';
      detailsHtml += '</li>'
      $details.html(detailsHtml);
    }

    if (type === 'wedding') {
      $details = $('<ul></ul>');
      var detailsHtml = '';
      detailsHtml += '<li>'+location.features.size_sqft+'sqft / '+ location.features.size_sqm+' m<sup>2</sup></li>';
      detailsHtml += '<li>'+location.features.indoor_banquet+' Indoor Banquet</li>';
      detailsHtml += '<li>'+location.features.outdoor_banquet+' Outdoor Banquet</li>';
      detailsHtml += '<li class="list-inlne">';
      detailsHtml += location.features.golf ? '<span>Golf</span>': '';
      detailsHtml += location.features.beach ? ', <span>Beach</span>' : '';
      detailsHtml += location.features.chapel ? ', <span>Chapel</span>' : '';
      detailsHtml += location.features.poolside ? ', <span>Poolside</span>' : '';
      detailsHtml += location.features.spa ? ', <span>Spa</span>' : '';
      detailsHtml += location.features.activities_center ? ', <span>Activities Center</span>' : '';
      detailsHtml += location.features.makeup ? ', <span>Make-up Services</span>': '';
      detailsHtml += location.features.florists ? ', <span>Florists</span>' : '';
      detailsHtml += '</li>'
      $details.html(detailsHtml);
    }

    $head.html($('<img/>', { src: location.img }));
    $body.html(
      '<i class="icon icon--brand-' + location.brand + '-i"></i>' +
      '<h2>' + location.title + '</h2>'
      );

    $body.append($details);

    $link.attr('href', location.link);

    $('.location').addClass('location--detailed');

    $('#closeMapDetail').on('click', hideDetails);
  }

  function hideDetails() {
    $('.location').removeClass('location--detailed');
    google.maps.event.trigger(map, 'resize');
  }

  return {
    init: init,
    onReady: onReady,
    filter: applyFilters
  }

})(jQuery, window, _events);

/* On Map Load */
function onGMapLoaded() {
  window._events.trigger('mapLoaded');
  _map.onReady();
  _events.trigger('viewChanged', { view: $('[data-view-type-default]').data('view-type-default') });
}

class Map {
  constructor() {
    init();
  }

  init() {
    console.log('map init');
  }
}

export default Map
