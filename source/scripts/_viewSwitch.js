import $ from 'jquery'
import events from './_events'
import MultiSelect from './_multiselect'

class ViewSwitch {
  constructor(c) {
    if (c) {
      this.element = c.element
      this.collection = c.collection
    }

    return this
  }

  get element() { return $(this._element) }
  set element(el) {
    this._element = el
    this.dataRef = this._element.match(/\[data-(.*?)\]/)[1]
    this.render()
  }

  get collection() { return this._collection }
  set collection(c) {
    this._collection = c
    this.render()
  }

  init() {
    $(this.element).on('click', (e) => {
      this.view = $(e.currentTarget).data(this.dataRef)
      this.collection.display = this.view

      this.render()
    })

    this.initialized = true
  }

  render() {
    if (!this.element) return
    if (!this.collection) return

    const $button = $(`[data-${this.dataRef}=${this.collection.display}]`)

    if (!this.initialized) {
      $button.addClass('button--active')
      return this.init()
    }

    // set active button class
    $(this.element).removeClass('button--active')
    $button.addClass('button--active')


  }
}

export default ViewSwitch
