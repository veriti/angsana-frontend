import $ from 'jquery'
import events from './_events'
import MapView from './_directionsMapView'

class Directions {
  constructor(c) {
    if (c) {
      this.entries = c.entries
      this.element = c.element
    }

    this.renderer = new MapView();

    events.on('filtersUpdated', filters => this.filters = filters )
    events.on('displayUpdated', display => this.display = display )

    return this
  }

  get element() { return $(this._selector).get(0) }
  set element(el) {
    this._selector = el
    this.render()
  }

  get property() { return this._property }
  set property(p) {
    this._property = p
    this.render()
  }

  get landmarks() { return this._landmarks }
  set landmarks(e) {
    this._landmarks = e
    this.render()
  }

  render() {
    if (!this.element) return
    if (!this.property) return
    if (!this.landmarks) return

    this.renderer.element = this.element
    this.renderer.property = this.property
    this.renderer.landmarks = this.landmarks
  }
}

export default Directions
