import $ from 'jquery'
import events from './_events'
import t from './_translation'

class GridView {
  constructor(el) {
    if (el) { this.element = el }

    return this
  }

  get element() { return this._element }
  set element(el) {
    this._element = el
    this.render()
  }

  get cards() { return this._cards }
  set cards(c) {
    this._cards = c
    this.render()
  }


  get entries() { return this._entries }
  set entries(e) {
    this._entries = e
    this.groupedEntries = e.reduce((a, location) => {
      a[location.country] = a[location.country] || []
      a[location.country].push(location)
      return a
    }, {});

    this.render()
  }

  get type() { return this._type }
  set type(t) {
    this._type = t
    this.render()
  }

  renderCard(entry, type) {
    const { brand, link, img, title, excerpt, booknow, features } = entry

    if (type === `event`) {
      return `
      <div class="card brand--${brand.toLowerCase()}">
        <div class="card__head">
          ${ img && `<a href="${link}"><img src="${img}" alt="${title}" /></a>`}
        </div>
        <div class="card__body">
          ${ link && `<h4><a href="${link}">${title}</a></h4>` }
          <hr class="rule rule--centered">
          ${ excerpt && `<p class="text-primary"><em>${excerpt}</em></p>` }
          <ul class="list-tabular">
            ${features.size_sqft*1 || features.size_sqm*1 ? `<li>
              ${features.size_sqft*1 ? `<span>${Number(features.size_sqft).toLocaleString()}</span>sqft` : ``}
              ${features.size_sqft*1 && features.size_sqm*1 ? `/` : ``}
              ${features.size_sqm*1 ? `<span>${Number(features.size_sqm).toLocaleString()}</span>m<sup>2</sup></li>` : ``}
            </li>` : ``}
            ${features.delegates && `<li><span>${features.delegates}</span> Total Delegates</li>` }
            ${features.indoor_banquet && `<li><span>${features.indoor_banquet}</span> Indoor Banquet</li>` }
            ${features.outdoor_banquet && `<li><span>${features.outdoor_banquet}</span> Outdoor Banquet</li>` }
            ${features.guest_rooms && `<li><span>${features.guest_rooms}</span> Guest Rooms</li>` }
            ${features.ballroom && `<li><span>${features.ballroom}</span> Ballroom</li>` }
            ${features.breakout && `<li><span>${features.breakout}</span> Breakout Rooms</li>` }
            <li class="icons">
              ${features.roundtable_cap*1 ? `<i class="icon icon--seats-a" title="Round Table"></i>` : ``}
              ${features.classroom_cap*1 ? `<i class="icon icon--seats-b" title="Classroom"></i>` : ``}
              ${features.boardroom_cap*1 ? `<i class="icon icon--seats-c" title="Boardroom"></i>` : ``}
              ${features.freestand_cap*1 ? `<i class="icon icon--seats-d" title="Freestand"></i>` : ``}
              ${features.theatre_cap*1 ? `<i class="icon icon--seats-e" title="Theatre"></i>` : ``}
              ${features.ushape_cap*1 ? `<i class="icon icon--seats-f" title="U-shaped"></i>` : ``}
            </li>
          </ul>
        </div>
      </div>
      `
    }

    if (type === `wedding`) {
      return `
      <div class="card brand--${brand.toLowerCase()}">
        <div class="card__head">
          ${ img && `<a href="${link}"><img src="${img}" alt="${entry.property}" /></a>`}
        </div>
        <div class="card__body">
          ${ link && `<h4><a href="${link}">${entry.property}</a></h4>` }
          <hr class="rule rule--centered">
          ${ excerpt && `<p class="text-primary"><em>${excerpt}</em></p>` }
          <ul class="list-tabular">
            ${features.size_sqft*1 || features.size_sqm*1 ? `<li>
              ${features.size_sqft*1 ? `<span>${Number(features.size_sqft).toLocaleString()}</span>sqft` : ``}
              ${features.size_sqft*1 && features.size_sqm*1 ? `/` : ``}
              ${features.size_sqm*1 ? `<span>${Number(features.size_sqm).toLocaleString()}</span>m<sup>2</sup></li>` : ``}
            </li>` : ``}
            ${features.golf ? `<li class="item-inline">Golf</li>` : ``}
            ${features.spa ? `<li class="item-inline">Spa</li>` : ``}
            ${features.beach ? `<li class="item-inline">Beach</li>` : ``}
            ${features.chapel ? `<li class="item-inline">Chapel</li>` : ``}
            ${features.poolside ? `<li class="item-inline">Poolside</li>` : ``}
            ${features.activities_center ? `<li class="item-inline">Activities Center</li>` : ``}
            ${features.makeup ? `<li class="item-inline">Makeup Services</li>` : ``}
            ${features.florist ? `<li class="item-inline">Florists</li>` : ``}
          </ul>
        </div>
        <div class="card__footer">
          <div class="card__actions">
            <a href="${link}">View details &gt;</a>
          </div>
        </div>
      </div>
      `
    }

    if (type === `hotel`) {
      return `
      <div class="card brand--${brand.toLowerCase()}">
        <div class="card__head">
          ${ img && `<a href="${link}"><img src="${img}" alt="${title}" /></a>`}
        </div>
        <div class="card__body">
          ${ link && `<h4><a href="${link}">${title}</a></h4>` }
          <hr class="rule rule--centered">
          ${ excerpt && `<p class="text-primary"><em>${excerpt}</em></p>` }
        </div>
        <div class="card__footer">
          <div class="card__actions">
            <a href="${booknow}">${t('Book now')} &gt;</a>
            <a href="${link}">${t('View details')} &gt;</a>
          </div>
        </div>
      </div>
      `
    }

    return `
    <div class="card">
      <div class="card__body">&#9888;
        <small>Invalid view type</small>
      </div>
    </div>
    `
  }

  render() {
    if (!this.element) return
    if (!this.entries) return
    if (!this.type) return

    const brand = window.SITE_BRAND || 'default'

    const brandOrder = {
      'default': ['banyan-tree', 'angsana', 'cassia', 'dhawa'],
      'banyan-tree': ['banyan-tree', 'angsana', 'cassia', 'dhawa'],
      'angsana': ['angsana', 'banyan-tree', 'cassia', 'dhawa']
    }

    const sortedCountries = Object.keys(this.groupedEntries)
      .sort((a, b) => {
        if (a.toLowerCase() === 'upcoming hotel') return 1
        if (b.toLowerCase() === 'upcoming hotel') return -1

        return a.toLowerCase().localeCompare(b.toLowerCase())
      })

    const $entries = sortedCountries.map((entryKey) => {

      // sort entries prioritising brand
      const order = brandOrder[brand.toLowerCase()]

      const entriesBrand = this.groupedEntries[entryKey].reduce((a, entry) => {
        entry.priority = order.indexOf(entry.brand.toLowerCase())
        a.push(entry)
        return a
      }, []).sort((a, b) => a.priority - b.priority)

      const viewMore = `
        <p class="text-center"><a href="#" class="anchor">${t('View More')}</a></p>
      `

      return `
      <div class="cards-set">
        <div class="cards-set__title">
          <div class="container">
            <div class="text-left">${entryKey}</div>
          </div>
        </div>
        <div class="cards">
          ${ entriesBrand.map(entry => this.renderCard(entry, this.type)).join('') }
          <div class="card card--filler"></div>
        </div>
      </div>
      ${ entryKey.toLowerCase() === 'upcoming hotel' ? viewMore : `` }
      `
    })

    $(this.element).html($entries)
  }
}

export default GridView
