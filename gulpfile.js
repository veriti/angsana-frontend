const path = require('path');
const gulp = require('gulp');
const hb = require('gulp-hb');
const postcss = require('gulp-postcss');
const gulpif = require('gulp-if');
const rollup = require('rollup');
const babel = require('rollup-plugin-babel');
const browserSync = require('browser-sync');
const handlebars = require('gulp-compile-handlebars');
const layouts = require('handlebars-layouts');

const host = browserSync.create();
const paths = {
  source: 'source',
  dest: 'dist',
  styles: ['source/styles/**/*.css', '!source/styles/**/_*.css'],
  libraries: [
    'source/scripts/**/*.js',
    '!source/scripts/**/_*.js',
    '!source/scripts/main.js',
    '!source/scripts/catalog.js',
    '!source/scripts/directions.js',
    '!source/scripts/gallery.js'
  ],
  scripts: {
    main: ['source/scripts/main.js', 'source/scripts/**/_*.js'],
    catalog: 'source/scripts/catalog.js',
    directions: 'source/scripts/directions.js',
    gallery: 'source/scripts/gallery.js'
  },
  templates: 'source/templates/*.html',
  partials: 'source/templates/**/*.hbs',
  images: 'source/images/**/*.{jpg,png,gif,svg}',
  fonts: 'source/fonts/*',
  misc: [ 'source/*.{jpg,png,ico,xml,txt}']
};

gulp.task('launch', () => {
  host.init({
    notify: false,
    server: paths.dest,
    port: 3333,
    reloadDebounce: 400
  });
});

gulp.task('html', (done) => {
  const hbStream = hb()
    .partials(paths.partials)
    .helpers(layouts)
    .data({ foo: 'bar' })

  return gulp.src(paths.templates)
    .pipe(hbStream)
      .on('error', done)
    .pipe(gulp.dest(paths.dest))
    .pipe(host.stream())
});

gulp.task('styles', (done) => {
  var plugins = [
    require("postcss-import")(),
    require("postcss-cssnext")(),
    require('postcss-reporter')
  ];
  return gulp.src(paths.styles)
    .pipe(postcss(plugins))
      .on('error', done)
    .pipe(gulp.dest(`${paths.dest}/assets/css`))
    .pipe(host.stream())
});

gulp.task('libraries', () => {
  return gulp.src(paths.libraries)
    .pipe(gulp.dest(`${paths.dest}/assets/js`))
    .pipe(host.stream())
});

gulp.task('scripts.catalog', async function () {
  const bundle = await rollup.rollup({
    input: paths.scripts.catalog,
    external: ['jquery'],
    plugins: [
      babel({
        exclude: 'node_modules/**'
      })
    ]
  });

  await bundle.write({
    file: 'dist/assets/js/catalog.js',
    format: 'iife',
    globals: { jquery: 'jQuery'},
    name: 'catalog',
    sourcemap: 'inline'
  });

  await host.reload();
});

gulp.task('scripts.directions', async function () {
  const bundle = await rollup.rollup({
    input: paths.scripts.directions,
    external: ['jquery'],
    plugins: [
      babel({
        exclude: 'node_modules/**'
      })
    ]
  });

  await bundle.write({
    file: 'dist/assets/js/directions.js',
    format: 'iife',
    globals: { jquery: 'jQuery'},
    name: 'directions',
    sourcemap: 'inline'
  });

  await host.reload();
});

gulp.task('scripts.gallery', async function () {
  const bundle = await rollup.rollup({
    input: paths.scripts.gallery,
    external: ['jquery'],
    plugins: [
      babel({
        exclude: 'node_modules/**'
      })
    ]
  });

  await bundle.write({
    file: 'dist/assets/js/gallery.js',
    format: 'iife',
    globals: { jquery: 'jQuery'},
    name: 'gallery',
    sourcemap: 'inline'
  });

  await host.reload();
});

gulp.task('scripts', async function () {
  const bundle = await rollup.rollup({
    input: paths.scripts.main[0],
    external: ['jquery', 'markerclusterer'],
    plugins: [
      babel({
        exclude: 'node_modules/**'
      })
    ]
  });

  await bundle.write({
    file: 'dist/assets/js/main.js',
    format: 'iife',
    globals: {
      jquery: 'jQuery',
      markerclusterer: 'MarkerClusterer'
    },
    name: 'main',
    sourcemap: false
  });

  await host.reload();
});

gulp.task('images', () => {
  // process images
  return gulp.src(paths.images)
    .pipe(gulp.dest(`${paths.dest}/assets/img`))
    .pipe(host.stream())
});

gulp.task('fonts', () => {
  // process fonts
  return gulp.src(paths.fonts)
    .pipe(gulp.dest(`${paths.dest}/assets/fonts`))
    .pipe(host.stream())
});

gulp.task('misc', () => {
  // process custom file types
  return gulp.src(paths.misc)
    .pipe(gulp.dest(`${paths.dest}`))
    .pipe(host.stream())
});

gulp.task('watch', () => {
  gulp.watch(paths.templates, ['html']);
  gulp.watch(paths.partials, ['html']);
  gulp.watch(paths.styles[0], ['styles']);
  gulp.watch(paths.libraries, ['libraries']);
  gulp.watch(paths.scripts.main[0], ['scripts']);
  gulp.watch(paths.scripts.main[1], [
    'scripts', 'scripts.catalog', 'scripts.directions', 'scripts.gallery'
  ]);
  gulp.watch(paths.scripts.catalog, ['scripts.catalog']);
  gulp.watch(paths.scripts.directions, ['scripts.directions']);
  gulp.watch(paths.scripts.gallery, ['scripts.gallery']);
  gulp.watch(paths.images, ['images']);
  gulp.watch(paths.fonts, ['fonts']);
  gulp.watch(paths.misc, ['misc']);
});

gulp.task('default', ['html', 'images', 'scripts', 'scripts.catalog', 'scripts.directions', 'scripts.gallery', 'libraries', 'styles', 'fonts', 'misc']);

gulp.task('serve', ['default', 'watch', 'launch']);
