const path = require('path');
const express = require('express');
const port = process.env.PORT || 3000;
const server = express();

server.get('/favicon.ico', function(req, res) {
  res.writeHead(200, { 'Content-Type': 'image/x-icon' });
  res.end();
});

server.use(express.static(path.resolve(__dirname, 'dist')));

server.listen(port, (err) => {
  if (err) {
    console.error(err);
  }
  console.info(`🌎 Listening on port ${port}. Open up http://localhost:${port}/ in your browser.`);
});
