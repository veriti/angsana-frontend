import $ from 'jquery'
import events from './_events'

class DetailView {
  constructor(el) {
    if (el) { this.element = el }

    return this
  }

  get element() { return this._element }
  set element(el) {
    this._element = el
    this.render()
  }

  get cards() { return this._cards }
  set cards(c) {
    this._cards = c
    this.render()
  }

  get entries() { return this._entries }
  set entries(e) {
    this._entries = e
    this.groupedEntries = e.reduce((a, location) => {
      a[location.country] = a[location.country] || []
      a[location.country].push(location)
      return a
    }, {});

    this.render()
  }

  get type() { return this._type }
  set type(t) {
    this._type = t
    this.render()
  }

  renderCard(entry, type) {
    const { brand, link, img, title, excerpt, description, booknow, features } = entry

    if (type === `event`) {
      return `
      <div class="card brand--${brand.toLowerCase()}">
        <div class="card__head">
          ${ img && `<a href="${link}"><img src="${img}" alt="${title}" /></a>`}
        </div>
        <div class="card__body">
          ${ link && `<h4><a href="${link}">${title}</a></h4>` }
          <hr class="rule rule--centered">
          ${ excerpt && `<p class="text-primary"><em>${excerpt}</em></p>` }
          <ul class="list-tabular">
            <li>${features.size_sqft*1 ? `<span>${features.size_sqft}</span>sqft / <span>${features.size_sqm}</span>m<sup>2</sup>` : `<span>-</span>`}</li>
            <li>${features.delegates*1 ? `<span>${features.delegates}</span> Total Delegates` : `<span>-</span>` }</li>
            <li>${features.indoor_banquet*1 ? `<span>${features.indoor_banquet}</span> Indoor Banquet` : `<span>-</span>` }</li>
            <li>${features.outdoor_banquet*1 ? `<span>${features.outdoor_banquet}</span> Outdoor Banquet` : `<span>-</span>` }</li>
            <li>${features.guest_rooms*1 ? `<span>${features.guest_rooms}</span> Guest Rooms` : `<span>-</span>` }</li>
            <li>${features.ballroom*1 ? `<span>${features.ballroom}</span> Ballroom` : `<span>-</span>` }</li>
            <li>${features.breakout*1 ? `<span>${features.breakout}</span> Breakout Rooms` : `<span>-</span>` }</li>
            <li class="icons">
              ${features.roundtable_cap*1 ? `<i class="icon icon--seats-a" title="Round Table"></i>` : ``}
              ${features.classroom_cap*1 ? `<i class="icon icon--seats-b" title="Classroom"></i>` : ``}
              ${features.boardroom_cap*1 ? `<i class="icon icon--seats-c" title="Boardroom"></i>` : ``}
              ${features.freestand_cap*1 ? `<i class="icon icon--seats-d" title="Freestand"></i>` : ``}
              ${features.theatre_cap*1 ? `<i class="icon icon--seats-e" title="Theatre"></i>` : ``}
              ${features.ushape_cap*1 ? `<i class="icon icon--seats-f" title="U-shape"></i>` : ``}
            </li>
          </ul>
        </div>
      </div>
      `
    }

    if (type === `wedding`) {
      return `
      <div class="card brand--${brand.toLowerCase()}">
        <div class="card__head">
          ${ img && `<a href="${link}"><img src="${img}" alt="${title}" /></a>`}
        </div>
        <div class="card__body">
          ${ link && `
            <h4>
              <a href="${link}" class="head-link">${entry.property}</a>
              <a href="${link}" class="head-link">
                <i class="icon icon--ui-camera"></i> View photos &gt;
              </a>
            </h4>
            `
          }
          <hr class="rule rule--centered">
          <ul class="list-tabular">
            ${ features.size_sqft && features.size_sqm ? `<li><span>${features.size_sqft}sqft / ${features.size_sqm}m<sup>2</sup></span></li>` : `` }
            ${ `<li class="item-inline">Golf<i class="icon icon--ui-${features.golf ? 'check' : 'dash'}"></i></li>` }
            ${ `<li class="item-inline">Beach<i class="icon icon--ui-${features.beach ? 'check' : 'dash'}"></i></li>` }
            ${ `<li class="item-inline">Chapel<i class="icon icon--ui-${features.chapel ? 'check' : 'dash'}"></i></li>` }
            ${ `<li class="item-inline">Poolside<i class="icon icon--ui-${features.poolside ? 'check' : 'dash'}"></i></li>` }
            ${ `<li class="item-inline">Garden<i class="icon icon--ui-${features.garden ? 'check' : 'dash'}"></i></li>` }
            ${ `<li class="item-inline">Spa<i class="icon icon--ui-${features.spa ? 'check' : 'dash'}"></i></li>` }
            ${ `<li class="item-inline">Activities Center<i class="icon icon--ui-${features.activities_center ? 'check' : 'dash'}"></i></li>` }
            ${ `<li class="item-inline">Make-up Services<i class="icon icon--ui-${features.makeup ? 'check' : 'dash'}"></i></li>` }
            ${ `<li class="item-inline">Florists<i class="icon icon--ui-${features.florist ? 'check' : 'dash'}"></i></li>` }
          </ul>
        </div>
        <div class="card__footer"></div>
      </div>
      `
    }

    if (type === `hotel`) {
      return `
      <div class="card brand--${brand.toLowerCase()}">
        <div class="card__body">
          ${ link && `<h4><a href="${link}">${title}</a></h4>` }
        </div>
      </div>
      `
    }

    return `
    <div class="card">
      <div class="card__body">&#9888;
        <small>Invalid view type</small>
      </div>
    </div>
    `
  }

  render() {
    if (!this.element) return
    if (!this.entries) return
    if (!this.type) return

    const brand = window.SITE_BRAND || 'default'

    const brandOrder = {
      'default': ['banyan-tree', 'angsana', 'cassia', 'dhawa'],
      'banyan-tree': ['banyan-tree', 'angsana', 'cassia', 'dhawa'],
      'angsana': ['angsana', 'banyan-tree', 'cassia', 'dhawa']
    }

    const sortedCountries = Object.keys(this.groupedEntries)
      .sort((a, b) => {
        if (a.toLowerCase() === 'upcoming hotel') return 1
        if (b.toLowerCase() === 'upcoming hotel') return -1

        return a.toLowerCase().localeCompare(b.toLowerCase())
      })

      console.log(sortedCountries)

    const $entries = sortedCountries.map((entryKey) => {

      // sort entries prioritising brand
      const order = brandOrder[brand.toLowerCase()]

      const entriesBrand = this.groupedEntries[entryKey].reduce((a, entry) => {
        entry.priority = order.indexOf(entry.brand.toLowerCase())
        a.push(entry)
        return a
      }, []).sort((a, b) => a.priority - b.priority)

      const typeMap = {
        event: 'cards-set--detail',
        wedding: 'cards-set--detail-a',
        hotel: 'cards-set--list',
      }

      const eventThead = `
        <div class="cards-thead">
          <div>Property</div>
          <div>Total Space</div>
          <div>Total Delegates</div>
          <div>Indoor Banquet</div>
          <div>Outdoor Banquet</div>
          <div>Guest Rooms</div>
          <div>Ballroom</div>
          <div>Breakout Rooms</div>
          <div>Seating Style</div>
        </div>
      `

      const weddingThead = `
        <div class="cards-thead">
          <div>Property</div>
          <div>Total Space</div>
          <div>Golf</div>
          <div>Beach</div>
          <div>Chapel</div>
          <div>Poolside</div>
          <div>Garden</div>
          <div>Spa</div>
          <div>Activities Center</div>
          <div>Make up Services</div>
          <div>Florists</div>
        </div>
      `

      return `
      <div class="cards-set ${typeMap[this.type]}">
        <div class="cards-set__title">
          <div class="container">
            <div class="text-left">${entryKey}</div>
          </div>
        </div>
        ${ this.type==='wedding' ? weddingThead : `` }
        ${ this.type==='event' ? eventThead : `` }
        <div class="cards">
          ${ entriesBrand.map(entry => this.renderCard(entry, this.type)).join('') }
          <div class="card card--filler"></div>
        </div>
      </div>
      `
    })

    $(this.element).html($entries)
  }
}

export default DetailView
