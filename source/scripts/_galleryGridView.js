import $ from 'jquery'

class GridView {
  constructor() {
    this.properties = window.BT_PROPERTIES || null
    this.$list = $('.gg-gridview')
    this.$buttonLoadMore = $('.gg-gridview__more button')

    this.render()
    this.bindings()
  }

  get limit() { return this._limit }
  set limit(e) {
    this._limit = e
    this.limitProperties = this.properties.slice(0, e)

    this.render()
  }

  bindings() {
    this.$buttonLoadMore.on('click', () => {
      this.limit = this.limit + 6
    })
  }

  render() {
    if (!this.limit) {
      this.limit = 6
    }

    const props = this.limitProperties

    this.gridHtml = props.map((property) => {
      const { img, link, title, excerpt } = property
      return (`
        <li style="background-image:url(${img})">
          <a href="${link}">
            <div class="gg-gridview__entry">
            ${title ? `<h5>${title}</h5>` : ``}
            ${excerpt ? `<p>${excerpt}</p>` : ``}
            </div>
          </a>
        </li>
      `)
    }).join('')

    // render list items
    $('.gg-gridview__list').html(this.gridHtml)

    // remove button if all is loaded
    if (this.limitProperties.length === this.properties.length) {
      $('.gg-gridview__more').remove()
    }
  }
}

export default GridView
