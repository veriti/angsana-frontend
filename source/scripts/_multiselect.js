import $ from 'jquery'
import { getUrlParams, capitalize } from './_helpers'
import events from './_events'

class MultiSelect {
  constructor(element, options, key) {
    this.$options = $(`<div class="multiselect__options"></div>`)
    this.$toggle = $(`<button type="button" class="multiselect__toggle"></button>`)
    this.$selectall = $(`<button type="button" class="multiselect__selectall"></button>`)

    this.element = element
    this.options = options
    this.key = key

    return this
  }

  get element() { return this._element }
  set element(el) {
    this._element = el
    this.render()
  }

  get options() { return this._options }
  set options(o) {
    this._options = o
    this.render()
  }

  get key() { return this._key }
  set key(k) {
    this._key = k
    this.render()
  }

  init() {
    $(this.element).html(this.$toggle)
    $(this.element).append(this.$options)
    this.$checkboxes = this.$options.find('input[type=checkbox]')
    this.$selectall.on('click', () => this.handleSelectAll())

    this.$checkboxes = $([])
    this.initialized = true
  }

  getButtonTitle() {
    const countAll = this.$checkboxes.length
    const countChecked = this.$checkboxes.filter(':checked').length

    if (countAll === countChecked) {
      return `All ${this.key}`
    }

    if (countChecked === 1) {
      return `${capitalize(this.$checkboxes.filter(':checked').val())}`
    }

    return `${countChecked} ${this.key}`
  }

  getSelectorTitle() {
    const countAll = this.$checkboxes.length
    const countChecked = this.$checkboxes.filter(':checked').length

    if (countAll === countChecked) {
      return `Select none`
    }

    return `Select all`
  }

  handleInputChange(e) {
    events.emit('filtersOptionsUpdated')

    this.$toggle.html(`<span>${this.getButtonTitle()}</span>`)
    this.$selectall.html(`<span>${this.getSelectorTitle()}</span>`)
  }

  handleSelectAll(e) {
    e && e.preventDefault()
    const countAll = this.$checkboxes.length
    const countChecked = this.$checkboxes.filter(':checked').length

    if (countAll === countChecked) {
      this.$checkboxes.prop('checked', false)
    } else {
      this.$checkboxes.prop('checked', true)
    }

    this.$checkboxes.trigger('change')
  }

  render() {
    if (!this.element) return
    if (!this.options) return
    if (!this.key) return

    if (!this.initialized) {
      this.init()
    }

    this.$toggle.html(`<span>All ${this.key}</span>`)
    this.$selectall.html(`<span>Select none</span>`)

    const params = getUrlParams(window.location.href)

    const fields = this.options.map(option => {
      let checked = true

      if (params[this.key]) {
        checked = params[this.key].includes(option)
      }

      const $label = $(`<label/>`).html(` ${capitalize(option)}`)
      const $input = $('<input/>', {
        type: `checkbox`,
        name: `${this.key}[]`,
        value: `${option}`,
        checked: checked
      })

      $input.on('change', () => this.handleInputChange())

      this.$checkboxes = this.$checkboxes.add($input)

      $label.prepend($input)

      return $label
    })

    this.$options.html(`<input type="hidden" name="${this.key}[]" value="">`)
    this.$options.append(fields)
    this.$options.append(this.$selectall)

    // update checkboxes
    this.$options.find('input').trigger('change')
  }
}

export default MultiSelect
