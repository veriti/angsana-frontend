import $ from 'jquery'
import events from './_events'
import mapTheme from './_mapTheme'

class MapRenderer {
  constructor() {
    this.markers = []
    this.apiUrl = window.BTMAP_URL || 'http://maps.google.cn/maps/api/js'
    this.apiKey = window.BTMAP_KEY || 'AIzaSyAJSD9TjIt4qrTs1MIOztDGXprEshyGPkw'
    this.zoomValue = window.ZOOM || 14
    this.mapConfig = {
      zoom: 3,
      center: { lat: -28.024, lng: 140.887 },
      styles: this.getMapStyle(),
      mapTypeControl: false,
      fullscreenControl: false,
      streetViewControl: false
    }

    return this
  }

  getMapStyle() {
    if (window.SITE_BRAND && window.SITE_BRAND === 'angsana') {
      return  mapTheme['angsana']
    }

    return mapTheme['default']
  }

  get element() { return this._element }
  set element(el) {
    this._element = el
    this.render()
  }

  get entries() { return this._entries }
  set entries(e) {
    this._entries = e
    this.render()
  }

  get map() { return this._map }
  set map(m) { this._map = m }

  init(callback) {
    if (!window.google && !this.gmapLoading) {
      const url = `${this.apiUrl}?key=${this.apiKey}`
      const api = $.getScript(url).then(() => {
        this.gmapLoading = false
        this.gmap = window.google.maps
        callback()
      })

      this.gmapLoading = true
    }
  }

  drawMap() {
    const $template = $(`
      <div class="location">
        <div class="map location__map"></div>
        <div class="location__detail"></div>
      </div>
    `)

    this.$view = $(this.element).html($template)
    this.$viewMap = this.$view.find('.location__map')
    this.$viewDetails = this.$view.find('.location__detail')

    if (this.map && this.map.getDiv()) {
      return this.$viewMap.html(this.map.getDiv())
    }

    this.map = new this.gmap.Map(this.$viewMap.get(0), this.mapConfig)
  }

  handleMarkerClick(marker, location) {
    this.buildDetailsHtml(location)

    this.gmap.event.trigger(this.map, 'resize')
    this.map.panTo(marker.getPosition())
    this.map.setZoom(this.zoomValue)
  }

  buildDetailsHtml(data) {
    const icon = data.brand ? `<i class="icon icon--brand-${data.brand.toLowerCase()}-i"></i>` : ``
    const image = data.img ? `<img src="${data.img}"/>` : ``
    const title = data.title ? `<h2>${data.title}</h2>` : ``
    const description = data.description ? `<p>${data.description}</p>` : ``
    const excerpt = data.excerpt ? `<p>${data.excerpt}</p>` : ``
    const link = data.link ? `<p><a href="${data.link}"><small>Discover more &gt;</small></a></p>` : ``
    const { features } = data
    const templates = {}

    templates.event = `
        <button class="button">
          <i class="icon icon--ui-close-i"></i>
        </button>
        <header>${image}</header>
        <article>
          ${icon}
          ${title}
          ${description}
          <ul>
            <li>${features && features.size_sqft ? `<span>${features.size_sqft}</span>sqft / <span>${features.size_sqm}</span>m<sup>2</sup>` : `` }</li>
            <li>${features && features.delegates ? `<span>${features.delegates}</span> Total Delegates` : `` }</li>
            <li>${features && features.indoor_banquet ? `<span>${features.indoor_banquet}</span> Indoor Banquet` : `` }</li>
            <li>${features && features.outdoor_banquet ? `<span>${features.outdoor_banquet}</span> Outdoor Banquet` : `` }</li>
            <li>${features && features.guest_rooms ? `<span>${features.guest_rooms}</span> Guest Rooms` : `` }</li>
            <li>${features && features.ballroom ? `<span>${features.ballroom}</span> Ballroom` : `` }</li>
            <li>${features && features.breakout ? `<span>${features.breakout}</span> Breakout Rooms` : `` }</li>
            <li class="icons">
              ${features && features.roundtable_cap ? `<i class="icon icon--seats-a"></i>` : ``}
              ${features && features.classroom_cap ? `<i class="icon icon--seats-b"></i>` : ``}
              ${features && features.boardroom_cap ? `<i class="icon icon--seats-c"></i>` : ``}
              ${features && features.freestand_cap ? `<i class="icon icon--seats-d"></i>` : ``}
              ${features && features.theatre_cap ? `<i class="icon icon--seats-e"></i>` : ``}
              ${features && features.ushape_cap ? `<i class="icon icon--seats-f"></i>` : ``}
            </li>
          </ul>
        </article>
        <footer>
          ${link}
        </footer>
      `

    let feats = []
    if (features && features.golf) feats.push('Golf')
    if (features && features.beach) feats.push('Beach')
    if (features && features.chapel) feats.push('Chapel')
    if (features && features.poolside) feats.push('Poolside')
    if (features && features.activities_center) feats.push('Activities Center')
    if (features && features.makeup) feats.push('Makeup Services')
    if (features && features.florist) feats.push('Florists')

    templates.wedding = `
        <button class="button">
          <i class="icon icon--ui-close-i"></i>
        </button>
        <header>${image}</header>
        <article>
          ${icon}
          ${title}
          ${excerpt}
          <p>${features && features.size_sqft ? `${features.size_sqft}sqft / ${features.size_sqm}m<sup>2</sup>` : ``}</p>
          <p>${feats.join(', ')}</p>
        </article>
        <footer>
          ${link}
        </footer>
      `

    templates.global = `
        <button class="button">
          <i class="icon icon--ui-close-i"></i>
        </button>
        <header>${image}</header>
        <article>
          ${icon}
          ${title}
          ${description}
        </article>
        <footer>
          ${link}
        </footer>
      `
    this.$viewDetails.html(templates[this.type] ? templates[this.type] : templates.global)
    this.$viewDetails.on('click', 'button', () => this.dismissDetails() )
  }

  dismissDetails() {
    this.$viewDetails.empty()
    this.gmap.event.trigger(this.map, 'resize')
  }

  unsetMarkers() {
    this.markers = [];
    this.markerClusterer && this.markerClusterer.clearMarkers();
  }

  setMarkers() {
    let iconPath = window.BTMAP_IMAGEPATH;
    let clusterSizes = [50, 70, 85, 105, 120];
    let clustererOptions = {
      styles: clusterSizes.map((size, i) => {
        return {
          url: iconPath + 'm' + (i + 1) + '.png',
          height: size,
          width: size,
          textSize: 20 + (i * 2),
          textColor: 'white'
        }
      })
    }

    this.bounds = new this.gmap.LatLngBounds()
    this.entries.map(this.createMarker, this)

    if (this.entries.length) {
      this.markerClusterer = new MarkerClusterer(this.map, this.markers, clustererOptions)
      this.map.fitBounds(this.bounds, 80)
      this.gmap.event.trigger(this.map, 'resize')
    }
  }

  createMarker(location) {
    let iconPath = window.BTMAP_IMAGEPATH;
    let marker = new this.gmap.Marker({
      position: location.latLng,
      icon: iconPath + 'pin-' + location.brand.toLowerCase() + '.png'
    });

    var infoWindow = new this.gmap.InfoWindow({
      content: location.title
    });

    marker.addListener('click', () => this.handleMarkerClick(marker, location));
    marker.addListener('mouseover', () => infoWindow.open(this.map, marker));
    marker.addListener('mouseout', () => infoWindow.close(this.map, marker));

    this.bounds.extend(marker.getPosition());
    this.markers.push(marker);

    return marker;
  }

  render() {
    if (!this.element) return
    if (!this.entries) return

    if (!this.gmap) {
      return this.init(() => {
        this.drawMap()
        this.setMarkers()
      })
    }

    this.drawMap()
    this.unsetMarkers()
    this.setMarkers()
  }
}

export default MapRenderer
