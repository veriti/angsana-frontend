import $ from 'jquery'

class ListView {
  constructor() {
    this.properties = window.BT_PROPERTIES || null
    this.$list = $('.gg-listview')

    this.render()
    this.bindEvents()
  }

  get filter() { return this._filter }
  set filter(e) {
    this._filter = e

    this.filteredProperties = this.properties.reduce( (a, property) => {
      if (e.country && e.country !== property.country) return a
      if (e.theme && e.theme !== property.destination) return a
      // if (property.country !== ) return a
      a.push(property)
      return a
    }, []);

    this.render()
  }

  get propertiesByCountry() {
    const props = this.filteredProperties || this.properties

    return props.reduce( (a, property) => {
      a[property.country] = a[property.country] || []
      a[property.country].push(property)
      return a
    }, {})
  }

  get propertiesByTheme() {
    const props = this.filteredProperties || this.properties

    return props.reduce( (a, property) => {
      a[property.destination] = a[property.destination] || []
      a[property.destination].push(property)
      return a
    }, {})
  }

  init() {
    this.$listFilter = $(
      `<form>
        <select name="country">
          <option value="">All countries</option>
        ${
          Object.keys(this.propertiesByCountry).map( (country) => {
            return `<option value="${country}">${country}</option>`
          }).join('')
        }
        </select>

        <select name="theme">
          <option value="">All themes</option>
        ${
          Object.keys(this.propertiesByTheme).map( (theme) => {
            return `<option value="${theme}">${theme}</option>`
          }).join('')
        }
        </select>
      <form>`
    )

    $('.gg-listview__select').html(this.$listFilter)
  }

  bindEvents() {
    this.$listFilter.on('submit', (e) => {
      e.preventDefault()

      const formValue = this.$listFilter.serializeArray()

      this.filter = formValue.reduce((a, result) => {
        a[result.name] = result.value
        return a
      }, {})
    })

    this.$listFilter.on('change', () => {
      this.$listFilter.submit()
    })

    this.$listFilter.find('select').chosen({
      disable_search: true
    })
  }

  render() {
    if (!this.$listFilter) {
      this.init()
    }

    this.listHtml = Object.keys(this.propertiesByCountry).map( country => (`
        <h4>${country}</h4>
        <ul>
        ${
          this.propertiesByCountry[country].map( property => {

            const { title, excerpt, img, link } = property

            return (`
              <li>
                <a href="${link}">
                  <div class="gg-listview__image"
                    ${img ? `style="background-image:url(${img})"` : ``}>
                  </div>
                  <div class="gg-listview__entry">
                    ${title ? `<h5>${title}</h5>` : ``}
                    ${excerpt ? `<p>${excerpt}</p>` : ``}
                  </div>
                </a>
              </li>
            `)
          }).join('')
        }
        </ul>
      `)
    ).join('')

    $('.gg-listview__list').html(`${this.listHtml}`)
  }
}

export default ListView
