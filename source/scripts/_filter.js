import $ from 'jquery'
import { arrayToObject } from './_helpers'
import events from './_events'
import MultiSelect from './_multiselect'

class Filter {
  constructor(c) {
    if (c) {
      this.element = c.element
      this.collection = c.collection
      this.dropdowns = c.dropdowns
    }

    return this
  }

  get element() { return $(this._element).get(0) }
  set element(el) {
    this._element = el
    this.render()
  }

  get dropdowns() { return $(this._dropdowns).toArray() }
  set dropdowns(o) {
    this._dropdowns = o
    this.render()
  }

  get collection() { return this._collection }
  set collection(c) {
    this._collection = c
    this.render()
  }

  getOptions(option) {
    const categoryMap = {
      brands: 'brand',
      countries: 'country',
      destinations: 'destination',
      themes: 'theme',
    }

    const options = this.collection.entries.reduce((a, entry) => {
      if (a.indexOf(entry[categoryMap[option]]) > -1) return a
      a.push(entry[categoryMap[option]])
      return a
    }, [])

    if (options.indexOf() === -1) {

      return options.sort((a, b) => {
        if (a.toLowerCase() === 'upcoming hotel') return 1
        if (b.toLowerCase() === 'upcoming hotel') return -1

        return a.toLowerCase().localeCompare(b.toLowerCase())
      })
    }

    return null
  }

  buildDropdowns() {
    const elementData = $(this.element).data()

    this.dropdowns.map(element => {
      const dataRef = this._dropdowns.match(/\[data-(.*?)\]/)[1]
      const dataKey = $(element).data(dataRef)
      const options = this.getOptions(dataKey)

      return new MultiSelect(element, options, dataKey)
    })
  }

  handleSubmit(e) {
    e && e.preventDefault()
    const formData = $(e.target).serializeArray()
    const formDataJson = formData.reduce((a, datum) => {
      const name = datum.name.replace(/[\[\]']+/g,'')
      a[name] = a[name] || []
      a[name].push(datum.value)
      return a
    }, {})

    events.emit('filtersUpdated', formDataJson)
  }

  init() {
    $(this.element).on('submit', this.handleSubmit)
    this.buildDropdowns()
  }

  render() {
    if (!this.element) return
    if (!this.collection) return
    if (!this.dropdowns.length) return

    this.init()

    if (!this.initialized) {
      $(this.element).find('form').trigger('submit')
      this.initialized = true
    }
  }
}

export default Filter
