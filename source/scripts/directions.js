import $ from 'jquery';
import MapView from './_directionsMapView'
import events from './_events'

$(function() {

  let bt = new MapView()
  bt.landmarks = window.BTMAP_LANDMARKS
  bt.property = window.BTMAP_PROPERTY
  bt.zoom = window.BTMAP_ZOOM
  bt.siteBrand = window.SITE_BRAND || 'default'
  bt.element = '[data-directions]'

  window.bt = bt
  window.events = events
})
