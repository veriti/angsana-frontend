import $ from 'jquery';
import events from './_events'
import Collection from './_collection'
import Filter from './_filter'
import ViewSwitch from './_viewSwitch'

$(function() {
  var cxn = window.__bt_collection = new Collection({
    element: '[data-collection]',
    entries: window.BTMAP_LOCATIONS || window.BTMAP_EVENTS || window.BTMAP_WEDDINGS,
    display: $('[data-collection]').data('collection')
  })

  window.__bt_filters = new Filter({
    element: '[data-filter]',
    collection: cxn,
    dropdowns: '[data-filter-dropdown]'
  })

  window.__bt_switch = new ViewSwitch({
    element: '[data-view-type]',
    collection: cxn
  })

  window.__bt_events = events
})
