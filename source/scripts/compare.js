String.prototype.class = function() {
  return this.substr(1);
}

var Compare = function() {

  var o = {
    compareEl: '.compare',
    compareTargetEl: '.compare__toggle',
    selectedEl: '.compare--selected',
    buttonEl: '#buttonCompare',
    countEl: '#buttonCompare span',
    modalEl: '#roomsModal',
    minCount: 2,
    maxCount: 3,
  }

  this.init = function() {
    this.compare = $(o.compareEl);
    this.button = $(o.buttonEl);
    this.counter = $(o.countEl);
    this.count = 0;
    _initCompare.bind(this)();
    _initBindings.bind(this)();
    _initModal.bind(this)();
  }

  var _doCount = function() {
    this.count = this.compare.filter(o.selectedEl).length;
    this.counter.html(this.count);
    this.button.prop('disabled', this.count < o.minCount);
  }

  var _initCompare = function() {
    var _this = this;

    this.compare.prepend('<div class="compare__select compare__toggle"><i class="icon icon--form-checkbox"></i><i class="icon icon--form-checkbox-checked"></i></div>');
  }

  var _initBindings = function() {
    var _this = this;

    this.compare
      .on('compare.reset', function() {
        $(this).removeClass(o.selectedEl.class());
        _doCount.bind(_this)();
      })
      .on('compare.select', function() {
        $(this).addClass(o.selectedEl.class());
      })
      .on('compare.deselect', function() {
        $(this).removeClass(o.selectedEl.class());
      })
      .on('click', o.compareTargetEl, function(e) {
        var $compare = $(e.delegateTarget);

        if ($compare.is(o.selectedEl)) {
          $compare.trigger('compare.deselect');
          _doCount.bind(_this)();
          return;
        }

        if (_this.count < o.maxCount) {
          $compare.trigger('compare.select');
          _doCount.bind(_this)();
          return;
        }

        alert('Cannot select more than 3');
      });

    this.button.on('click', function() {
      var $selection = _this.compare.filter(o.selectedEl).clone();

      // cleanup
      $selection.find('.card__footer').remove();
      $selection.find('.compare__select').remove();

      _this.modalBody.html($selection);
      _this.modal.modal();
    })
  }

  var _initModal = function() {
    this.modal = $(o.modalEl);
    this.modalBody = this.modal.find('.modal-body')
  }

  this.init();
}
