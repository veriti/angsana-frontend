import $ from 'jquery'
import events from './_events'
import mapTheme from './_mapTheme'

class MapView {
  constructor() {
    this.markers = []
    this.apiUrl = window.BTMAP_URL || 'http://maps.google.cn/maps/api/js'
    this.apiKey = window.BTMAP_KEY || 'AIzaSyAJSD9TjIt4qrTs1MIOztDGXprEshyGPkw'
    this.siteBrand = window.SITE_BRAND || 'default'
    this.mapConfig = {
      zoom: 3,
      center: { lat: -28.024, lng: 140.887 },
      styles: mapTheme[this.siteBrand],
      mapTypeControl: false,
      fullscreenControl: false,
      streetViewControl: false
    }

    return this
  }

  get element() { return this._element }
  set element(el) {
    this._element = el
    this.render()
  }

  get landmarks() { return this._landmarks }
  set landmarks(e) {
    this._landmarks = e
    this.render()
  }

  get property() { return this._property }
  set property(e) {
    this._property = e
    this.render()
  }

  get zoom() { return this._zoom || 10 }
  set zoom(z) {
    this._zoom = z
    this.map && this.map.setZoom(z)
  }

  get map() { return this._map }
  set map(m) {
    this._map = m
  }

  async init() {
    if (!window.google) {
      const url = `${this.apiUrl}?key=${this.apiKey}`
      const api = await $.getScript(url).then()
      this.gmap = window.google.maps
    }

    let $container = $('<div/>', { class: 'location' })
    let $map = $('<div/>', { class: 'map location__map' })

    $(this.element).html($container.html($map))

    this.map = new this.gmap.Map($map.get(0), this.mapConfig)
  }

  unsetMarkers() {
    this.markers = [];
  }

  setMarkers() {
    this.bounds = new this.gmap.LatLngBounds();
    this.property.map(this.createPropertyMarker, this);
    this.landmarks.map(this.createLandmarkMarker, this);
    this.map.fitBounds(this.bounds);
    this.gmap.event.addListenerOnce(this.map, 'idle', () => this.map.setZoom(this.zoom))
  }

  createPropertyMarker(location) {
    const iconPath = window.BTMAP_IMAGEPATH;
    const marker = new this.gmap.Marker({
      position: location.latLng,
      map: this.map,
      icon: iconPath + 'pin-' + location.brand + '.png'
    });

    const infoWindow = new this.gmap.InfoWindow({
      content: location.title
    });

    marker.addListener('mouseover', () => infoWindow.open(this.map, marker))
    marker.addListener('mouseout', () => infoWindow.close(this.map, marker))
    marker.addListener('click', () => this.map.panTo(marker.getPosition()))

    this.bounds.extend(marker.getPosition())
    this.markers.push(marker)
    this.propertyMarker = marker

    return marker;
  }

  createLandmarkMarker(location) {
    const iconPath = window.BTMAP_IMAGEPATH;
    const marker = new this.gmap.Marker({
      position: location.latLng,
      map: this.map,
      icon: {
        url: location.icon,
        scaledSize: new google.maps.Size(32, 32),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(16, 16)
      }
    });

    const infoWindow = new this.gmap.InfoWindow({
      content: location.title
    });

    marker.addListener('mouseover', () => infoWindow.open(this.map, marker))
    marker.addListener('mouseout', () => infoWindow.close(this.map, marker))
    marker.addListener('click', () => {
      const bounds = new this.gmap.LatLngBounds()
      bounds.extend(this.propertyMarker.getPosition())
      bounds.extend(marker.getPosition())
      this.map.fitBounds(bounds);
    });

    this.markers.push(marker);

    return marker;
  }

  async render() {
    if (!this.element) return
    if (!this.property) return
    if (!this.landmarks) return

    await this.init()

    this.unsetMarkers()
    this.setMarkers()
  }
}

export default MapView
