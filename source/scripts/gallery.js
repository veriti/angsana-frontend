import $ from 'jquery'
import HeroView from './_galleryHeroView'
import ListView from './_galleryListView'
import GridView from './_galleryGridView'

$(function() {
  window.__bt_galleryHero = new HeroView()
  window.__bt_galleryList = new ListView()
  window.__bt_galleryGrid = new GridView()
});
