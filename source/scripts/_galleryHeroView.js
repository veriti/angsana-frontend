import $ from 'jquery'

class HeroView {
  constructor() {
    this.properties = window.BT_PROPERTIES || null
    this.$themeTitle = $('.gg-gallery__switch p')
    this.$parent = $('.gg-hero')
    this.$entry = $('[data-entry]')
    this.$intro = $('.gg-intro')

    this.rebuild = true
    this.render()
    this.bindEvents()
  }

  get theme() { return this._theme }
  set theme(e) {
    this._theme = e

    this.filteredProperties = this.properties.reduce( (a, property) => {
      if (property.destination.toLowerCase() !== e) return a

      a.push(property)
      return a
    }, []);

    // rebuild only when necessary
    this.rebuild = this.filteredProperties.length
    this.currentId = 0
  }

  get currentId() { return this._id }
  set currentId(id) {
    this._id = id
    this.render()
  }

  init() {
    const props = this.filteredProperties || this.properties

    this.$thumbs = $(props.map((property, i) => (
      `<a data-slide="${i}"
        ${ property.title ? `title="${property.title}"` : `` }
        ${ property.img ? `style="background-image: url(${property.img})"` : `` }
      ></a>
      `
    )).join(''))

    this.$previews = $(props.map((property, i) => (
      `<div data-preview="${i}" style="background-image: url(${property.img})"></div>`
    )).join(''))

    $('.gg-gallery__thumbs').html(this.$thumbs)
    $('.gg-gallery__images').html(this.$previews)
  }

  bindEvents() {
    $(document).on('click', '[data-slide]', (e) => {
      e.preventDefault()
      this.currentId = $(e.target).data('slide')
    })

    $(document).on('click', '[data-theme]', (e) => {
      e.preventDefault()
      this.theme = $(e.currentTarget).data('theme')
    })
  }

  updateImage() {
    const id = this.currentId || 0
    let img = this.filteredProperties && this.filteredProperties.length
      && this.filteredProperties[id].img

    this.$thumbs.removeAttr('class')
    this.$thumbs.filter(`[data-slide=${id}]`).addClass('active')

    this.$previews.removeAttr('class')
    this.$previews.filter(`[data-preview=${id}]`).addClass('active')

    if (img) {
      this.$intro.attr('style', `background-image:url(${img})`)
      return
    }
  }

  updateEntry() {
    if (!this.filteredProperties) return

    const { title, excerpt, description, link } = this.filteredProperties[this.currentId]
    const desc = /<[a-z][\s\S]*>/i.test() ? description : `<p>${description}</p>`
    const entryHtml = `
      ${title ? `<h1>${title}` : `` }</h1>
      ${excerpt ? `<p class="lead">${excerpt}` : `` }</p>
      ${description ? `<div>${desc}` : `` }</div>
      <p><a class="button button--primary" href="${link || `#`}">explore more</a></p>
    `

    this.$entry.html(entryHtml)
  }

  updateGallery() {
    const method = this.theme ? 'addClass' : 'removeClass'

    this.$parent[method]('gg-hero--gallery')

    if (this.theme) {
      const title = $(`[data-theme=${this.theme}]`).text()
      const $icon = $(`<i class="icon icon--gallery-${this.theme}-i"></i>`)

      this.$themeTitle.html($icon)
      this.$themeTitle.append(title)
    }
  }

  render() {
    if (this.rebuild) {
      this.init()
      this.rebuild = false
    }

    this.updateGallery()
    this.updateImage()
    this.updateEntry()
  }
}

export default HeroView