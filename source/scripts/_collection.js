import $ from 'jquery'
import events from './_events'
import MapView from './_mapView'
import GridView from './_gridView'
import DetailView from './_detailView'

class Collection {
  constructor(c) {
    this.views = {
      map: new MapView(),
      detail: new DetailView(),
      grid: new GridView()
    }

    events.on('filtersUpdated', filters => this.filters = filters )
    events.on('entriesUpdated', entries => this.entries = entries )
    events.on('displayUpdated', display => this.display = display )

    if (c) {
      this.entries = c.entries
      this.element = c.element
      this.display = c.display
    }

    return this
  }

  get element() { return $(this._selector).get(0) }
  set element(el) {
    this._selector = el
    this.type = el
    this.render()
  }

  get type() { return $(`[data-${this._type}]`).data(`${this._type}`) }
  set type(el) {
    this._type = `${el.match(/\[data-(.+)\]/)[1]}-type`
  }

  get entries() { return this._entries }
  set entries(e) {
    this._entries = e
    this.render()
  }

  get display() { return this._display }
  set display(v) {
    if (this._display === v) return
    this._display = v
    this.renderer = this.views[this._display]
    this.render()
  }

  get filters() { return this._filters }
  set filters(f) {
    this._filters = f
    this.render()
  }

  get filteredEntries() {
    const e = this.entries.reduce((a, entry) => {

      if (this.filters) {

        // filter brands
        if (this.filters.brands
          && !this.filters.brands.includes(entry.brand)) return a

        // filter countries
        if (this.filters.countries
          && !this.filters.countries.includes(entry.country)) return a

        // filter destinations
        if (this.filters.destinations
          && !this.filters.destinations.includes(entry.destination)) return a

        // filter themes
        if (this.filters.themes
          && !this.filters.themes.includes(entry.theme)) return a

        // filter delegates
        if (this.filters.delegates && this.filters.delegates !== -1) {
          const range = this.filters.delegates[0].split(',')
          const d = entry.features.delegates

          if (range.length > 1) {
            if (d < parseInt(range[0])) return a
            if (d > parseInt(range[1])) return a
          }
        }

      }

      a.push(entry)
      return a
    }, []);

    return e

  }

  render() {
    if (!this.display) return
    if (!this.element) return
    if (!this.entries) return
    if (!this.type) return

    this.renderer.element = this.element
    this.renderer.entries = this.filteredEntries
    this.renderer.type = this.type
  }
}

export default Collection
